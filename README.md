# FliteTrakr

Based on a given set of flight paths and their respective prices this utility should be able to calculate best and cheapest flight paths. 

Original problem description: https://bitbucket.org/adigsd/backend-flitetrakr

## Design considerations
I want to make the application traverse the whole flights graph upfront and pre-calculate a data set in order to be able to answer all questions afterwards
efficiently without traversing the graph again.

## Status

My solution isn't finished, yet. 

Todo:

- A few parts of traversals/pre-calculations of the flight graph are missing --> thus the 'data set' of connections isn't complete and the answers cannot be delivered completely.
- Unit test coverage is too low.
- javadoc comments need to be completed.
- Running the JAR file leads to an 'Exception in thread "main" java.lang.NoClassDefFoundError' --> running via gradle works!

## Dependencies
- Java 8
- gradle  (https://gradle.org/install): 'Gradle helps teams build, automate and deliver better software, faster.'
- FreeBuilder (https://github.com/google/FreeBuilder): Builder pattern
- Guave (https://github.com/google/guava): data structures, Preconditions for checks.
- JUnit (http://junit.org/junit4/javadoc/latest/index.html): Java unit test framework
- hamcrest (https://code.google.com/archive/p/hamcrest): Matcher objects e.g. for unit tests.

## Questions
- Are the typos in the set of questions from the 'original problem description' intended?

## How to run application

./gradlew run

## How to run tests

./gradlew test