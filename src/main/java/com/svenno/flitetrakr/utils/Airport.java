package com.svenno.flitetrakr.utils;

import com.google.common.base.Preconditions;
import org.inferred.freebuilder.FreeBuilder;

@FreeBuilder
public interface Airport {

    /** IATA-code of the airport. */
    String iataCode();

    /** Returns a new {@link Builder} with the same property values as this Airport. */
    Builder toBuilder();

    /** Builder of {@link Airport} instances. */
    public class Builder extends Airport_Builder {
        @Override public Builder iataCode(String iataCode) {
	    super.iataCode(iataCode);
	    Preconditions.checkArgument(iataCode().length() == 3,
					"Invalid IATA code detected: >%s<!", iataCode());
	    return this;
	}
    }
}
