package com.svenno.flitetrakr.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileReader {

    private Scanner scanner;

    public FileReader(String fileName) throws IllegalStateException {
	try {
	    scanner = new Scanner(new File(fileName));
	} catch (FileNotFoundException e) {
	    throw new IllegalStateException("File not found");
	}
    }
    
    public boolean hasNext() {
        return scanner.hasNext();
    }
    
    public String nextLine() {
        return scanner.nextLine();
    }
    
    public void close() {
        scanner.close();
    }
}
