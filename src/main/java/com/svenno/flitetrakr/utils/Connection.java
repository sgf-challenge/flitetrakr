package com.svenno.flitetrakr.utils;

import org.inferred.freebuilder.FreeBuilder;

import com.svenno.flitetrakr.utils.Airport;
import com.svenno.flitetrakr.utils.Path;

@FreeBuilder
public interface Connection {

    /** Departure airport */
    Airport start();

    /** Arrival airport */
    Airport end();

    /** Connecting path between and inclusive start() and end(). */
    Path path();

    /** Cost for the trip from start() to end(). */
    Integer price();

    /** Cost for the round-trip from start() to start(), 0 otherwise. */
    Integer roundTripPrice();

    /** Number of stops between start() and end(), 0 otherwise. */
    Integer numberOfStops();

    /** Returns a new {@link Builder} with the same property values as this Connection. */
    Builder toBuilder();

    /** Builder of {@link Connection} instances. */
    public class Builder extends Connection_Builder {}
}
