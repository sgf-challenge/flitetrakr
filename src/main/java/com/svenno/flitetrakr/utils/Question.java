package com.svenno.flitetrakr.utils;

import com.google.common.base.Preconditions;
import org.inferred.freebuilder.FreeBuilder;
import java.util.regex.Matcher;

import com.svenno.flitetrakr.utils.PatternMatcher;

@FreeBuilder
public interface Question {

    /** Question text */
    String text();
    /** Pattern matcher to determin question type */
    PatternMatcher matcher();
    /** Determined question type */
    Integer type();
    
    /** Returns a new {@link Builder} with the same property values as this Question. */
    Builder toBuilder();

    /** Builder of {@link Question} instances. */
    public class Builder extends Question_Builder {

	public Builder text(String text) {
	    Preconditions.checkArgument(text.length() > 0,
					"Invalid question detected: >%s<!", text);
	    return super.text(text);
	}

	@Override public Question build() {

	    this.type(matcher().UNKNOWN);
	    
	    for (int i=0; i<matcher().MAX; i++) {
		Matcher matcher = matcher().getPreCompiledPattern(i, false).matcher(text());
		if (matcher.matches()) {
		    type(new Integer(i));
		    break;
		}
	    }
	    Question question = super.build();
	    return question;
	}
    }
}
