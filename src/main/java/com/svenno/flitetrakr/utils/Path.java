package com.svenno.flitetrakr.utils;

import com.google.common.base.Preconditions;
import org.inferred.freebuilder.FreeBuilder;

import java.util.List;
import com.svenno.flitetrakr.utils.Airport;

@FreeBuilder
public interface Path {

    /** Path as list of airports */
    List<Airport> airports();

    /** Flight Path as String */
    String pathAsString();
    
    /** Connection as String */
    String connectionAsString();
    
    /** Returns a new {@link Builder} with the same property values as this Path. */
    Builder toBuilder();

    /** Builder of {@link Path} instances. */
    public class Builder extends Path_Builder {

	@Override public Path build() {

            pathAsString("");
            connectionAsString("");
            String code = "";
            for (Airport airport: airports()) {
                code = airport.iataCode();
                if (pathAsString().length() > 0) {
                    pathAsString(pathAsString() + "-");
                } else {
                    connectionAsString(code);
                }
                pathAsString(pathAsString() + code);
	    }
            connectionAsString(connectionAsString() + "-");
            connectionAsString(connectionAsString() + code);
	    Path path = super.build();
	    Preconditions.checkArgument(airports().size() >= 2,
					"Invalid path detected: >%s<!", pathAsString());
	    return path;
	}
    }
}
