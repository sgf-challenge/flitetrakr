package com.svenno.flitetrakr.utils;

import com.google.common.base.Preconditions;
import java.util.regex.Pattern;

public class PatternMatcher {

    /** question types to be detected */
    public static final int PRICE_OF_CONNECTION = 0;
    public static final int CHEAPEST_CONNECTION = 1;
    public static final int CONNECTIONS_WITH_MAXIMUM_X_STOPS = 2;
    public static final int CONNECTIONS_WITH_MINIMUM_X_STOPS = 3;
    public static final int CONNECTIONS_WITH_EXACT_X_STOPS = 4;
    public static final int CONNECTIONS_BELOW_PRICE = 5;
    public static final int UNKNOWN = 6;
    public static final int MAX = 7;

    /** RegExp for question type detection */
    private static final String[] regexpQuestion = {

    // What is the price of the connection NUE-FRA-LHR?
	"What is .* price .* connection .*",
    // What is the cheapest connection from NUE to AMS?
	"What is .* cheapest connection .*",
    // How many different connections with maximum 3 stops exists between NUE and FRA?
	"How .* different connections with maximum.* ?[0-9]+ .* between .*",
    // How many different connections with minimum 1 stop exist between FRA and LHR?
	"How .* different connections with minimum.* ?[0-9]+ .* between .*",
    // How many different connections with exactly 1 stop exists between LHR and AMS?
	"How .* different connections with exactly.* ?[0-9]+ .* between .*",
    // Find all connections from NUE to LHR below 170Euros!	
	"Find all .* ?below .* ?[0-9]+.*",
    // UNKNOWN
	""
    };

    /** RegExp for retreiving data from question */
    private static final String[] regexpData = {

    // What is the price of the connection NUE-FRA-LHR?
	".* ([A-Z][A-Z][A-Z](-[A-Z][A-Z][A-Z])+).*",
    // What is the cheapest connection from NUE to AMS?
	".* ([A-Z][A-Z][A-Z]) .* ([A-Z][A-Z][A-Z]).*",
    // How many different connections with maximum 3 stops exists between NUE and FRA?
	".* ([0-9]+) .* ([A-Z][A-Z][A-Z]) .* ([A-Z][A-Z][A-Z]).*",
    // How many different connections with minimum 1 stop exist between FRA and LHR?
	".* ([0-9]+) .* ([A-Z][A-Z][A-Z]) .* ([A-Z][A-Z][A-Z]).*",
    // How many different connections with exactly 1 stop exists between LHR and AMS?
	".* ([0-9]+) .* ([A-Z][A-Z][A-Z]) .* ([A-Z][A-Z][A-Z]).*",
    // Find all connections from NUE to LHR below 170Euros!	
	".* ([A-Z][A-Z][A-Z]) .* ([A-Z][A-Z][A-Z]) .* ([0-9]+) ?[Ee]uro.*",
    // UNKNOWN
	""
    };

    /** Storing pre-compiled patterns to re-use them */
    private static Pattern[] preCompiledPatternQuestion = new Pattern[7]; // length todo
    private static Pattern[] preCompiledPatternData     = new Pattern[7]; // length todo
    
    public PatternMatcher() {
	PatternPreCompile();
    }

    private void PatternPreCompile() {
	for (int i=0; i<regexpQuestion.length; i++) {
	    preCompiledPatternQuestion[i] = Pattern.compile(regexpQuestion[i]);
	    preCompiledPatternData[i]     = Pattern.compile(regexpData[i]);
	}
    }

    public Pattern getPreCompiledPattern(int questionType, Boolean forData) {
	Preconditions.checkArgument(questionType >= 0 && questionType < regexpQuestion.length);
	if (forData) {
	    Preconditions.checkNotNull(preCompiledPatternData);
	    Preconditions.checkElementIndex(questionType, preCompiledPatternData.length);
	    return preCompiledPatternData[questionType];
	} else {
	    Preconditions.checkNotNull(preCompiledPatternQuestion);
	    Preconditions.checkElementIndex(questionType, preCompiledPatternQuestion.length);
	    return preCompiledPatternQuestion[questionType];
	}
    }
}
