package com.svenno.flitetrakr;

import com.google.common.collect.HashMultimap;
import com.google.common.graph.MutableValueGraph;
import com.google.common.base.Preconditions;
import com.google.common.graph.ValueGraphBuilder;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.List;
import java.util.ListIterator;
import java.util.LinkedList;
import java.util.regex.Matcher;

import com.svenno.flitetrakr.utils.Path;
import com.svenno.flitetrakr.utils.Airport;
import com.svenno.flitetrakr.utils.Connection;
import com.svenno.flitetrakr.utils.PatternMatcher;
import com.svenno.flitetrakr.utils.Question;
import com.svenno.flitetrakr.utils.FileReader;

/**
 * FlightTrakr service
 */
public class Service {

    /** Value Graph storing the given non-stop flights */
    private MutableValueGraph<Airport, Integer> flights;

    /** Multimap storing ALL possible connections (non-stop and stop-over flights) */
    private HashMultimap<String, Connection> connections;

    /** Pattern matcher used for matching the questions to type of question */
    private PatternMatcher patternMatcher;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Convert graph to 'dot' format (https://en.wikipedia.org/wiki/DOT_(graph_description_language))
    // private String graphAsString = "echo \"digraph {";
    
    Service() {
	// NOTE: initial sizes of graph and maps are too small for bigger scenarios
	flights           = ValueGraphBuilder.directed()
	                                     .allowsSelfLoops(false)
	                                     .expectedNodeCount(10)
	                                     .build();
	connections       = HashMultimap.create(50, 5);

	// pre-compiled patterns
	patternMatcher    = new PatternMatcher(); 

	// check 'em
	patternMatcher    = Preconditions.checkNotNull(patternMatcher);
	connections       = Preconditions.checkNotNull(connections);
    }

    private Connection storeConnectionData(String connectionAsString) throws IllegalArgumentException {
	final String[] connectionParts = connectionAsString.split("-");
	if (connectionParts.length != 3) {
	    throw new IllegalArgumentException("Wrong Connections Format detected");
	}

	final String  start = connectionParts[0];
	final String  end   = connectionParts[1];
        final String  priceStr = connectionParts[2];
	final Integer price = Integer.parseInt(priceStr);
	
	final Airport departure = new Airport.Builder()
	                                     .iataCode(start)
	                                     .build();
	final Airport arrival   = new Airport.Builder()
	                                     .iataCode(end)
	                                     .build();

	final Path        path  = new Path.Builder().addAirports(departure, arrival).build();
	final Connection connection = new Connection.Builder()
	                                            .start(departure)
	                                            .end(arrival)
	                                            .path(path)
	                                            .price(price)
	                                            .roundTripPrice(0)
	                                            .numberOfStops(0)
	                                            .build();
	
        /** add the non-stop flights to the connections data set */
        Preconditions.checkArgument(connections.put(path.connectionAsString(), connection));
	flights.putEdgeValue(departure, arrival, price);

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Convert graph to dot format (https://en.wikipedia.org/wiki/DOT_(graph_description_language))
        // graphAsString += start + "->" + end + "[label=\"" + priceStr + "\"];";

	return connection;
    }

    /// A longer connection gets separated into smaller sub trips, like
    /// NUE-FRA-LHR-NUE --> FRA-LHR-NUE
    /// and gets added to the pool of possible connections.
    public void addSubTrips(Connection trip) {
        System.out.println("addSubTrips: " + trip.path().pathAsString());
        System.out.println("----------------------------------------------------------");
        List<Airport> airportsToBeRemoved = trip.path().airports();
        Integer price = 0;
        for (int i=1; i < trip.path().airports().size()-2; i++) {
            printOutConnections();
            List<Airport> airports = trip.path().airports().subList(i, trip.path().airports().size());
            Airport removedAirport = airportsToBeRemoved.get(i-1);
            Airport airport = airports.get(0);
            Airport next = airports.get(1);
            Path shortPath = new Path.Builder().addAirports(removedAirport, airport).build();
            price += getExactThatConnectionsWithSamePath(removedAirport.iataCode(), airport.iataCode(), shortPath).price();
            Path tripPath = new Path.Builder().addAllAirports(airports).build();
	    Connection newSubTrip = new Connection.Builder()
	                                           .start(airport)
                                                   .end(trip.end())
						   .path(tripPath)
                                                   .price(trip.price()-price)
	                                           .roundTripPrice(0)
                                                   .numberOfStops(tripPath.airports().size()-2)
		                                   .build();

            String key = newSubTrip.path().connectionAsString();
            if (connections.put(key, newSubTrip)) {
                System.out.println("to be added1: " + newSubTrip.path().pathAsString() + " key: " + key);
            }
        }
    }
    
    /// A round trip (loop) can start at each airport of the loop, thus all possible loops
    /// will be created and added to the pool of possible connections, like:
    /// NUE-FRA-LHR-NUE --> FRA-LHR-NUE-FRA --> LHR-NUE-FRA-LHR
    public void addRoundTrips(Path path, Integer roundTripPrice) {
        Path newPath = path;
        for (int i=0; i < path.airports().size()-2; i++) {
            List<Airport> airports = newPath.airports().subList(1, newPath.airports().size());
            Airport airport = airports.get(0);
            newPath  = new Path.Builder()
                                    .addAllAirports(airports)
                                    .addAirports(airport)
                                    .build();
	    Connection roundTrip = new Connection.Builder()
	                                           .start(airport)
						   .end(airport)
						   .path(newPath)
		                                   .price(roundTripPrice)
	                                           .roundTripPrice(roundTripPrice)
                                                   .numberOfStops(path.airports().size()-2)
		                                   .build();

            String key = newPath.connectionAsString();
            if (connections.put(key, roundTrip)) {
                System.out.println("to be added2: " + roundTrip.path().pathAsString() + " key: " + key);
                addSubTrips(roundTrip);
            }
        }
    }
    
    public Connection traverseFlightGraph(MutableValueGraph<Airport, Integer> graph,
					  Connection connection) {
        System.out.println("----------------------------------------------------------");
        System.out.println("traverseFlightGraph: " + connection.path().pathAsString());
        final Airport currentAirport = connection.end();
	for (Airport next : graph.successors(currentAirport)) { // IllegalArgumentException
	    final Integer price = graph.edgeValueOrDefault(currentAirport, next, 0);
            final Path newPath  = connection.path().toBuilder().addAirports(next).build();

	    Integer roundTripPrice = 0;
            if (connection.start().equals(next) || price == 0) {
                // round-trip (like NUE-...-NUE) detected, calculate price per round-trip.
                roundTripPrice = connection.price() + price;
                System.out.println("round-trip detected, add all stops as round-trips");
                // add each round-trip stop as possible new round-trip connection 
                addRoundTrips(newPath, roundTripPrice);
            }
	    final Connection newConnection = new Connection.Builder()
	                                           .start(connection.start())
						   .end(next)
						   .path(newPath)
		                                   .price(connection.price() + price)
	                                           .roundTripPrice(roundTripPrice)
						   .numberOfStops(connection.numberOfStops()+1)
		                                   .build();

            final String key = newPath.connectionAsString();
            if (connections.put(key, newConnection)) {
                System.out.println("to be added3: " + newConnection.path().pathAsString() + " key: " + key);
            }

            if (roundTripPrice > 0) {
                System.out.println("because of round-trip, do not traverse this path further");
                continue;
            }

            // lookup next node
            traverseFlightGraph(graph, newConnection);
	}
        System.out.println("return: " + connection.path().pathAsString());
        System.out.println("----------------------------------------------------------");
	return connection;
    }

    /**
     * 
     *
     * Connection: NUE-FRA-43, NUE-AMS-67, FRA-AMS-17, FRA-LHR-27, LHR-NUE-23
     */
    public void addConnections(FileReader reader) {
        final String startSequence = "Connection: ";
        final String line = reader.nextLine();

        if (!line.startsWith(startSequence)) {
            throw new IllegalArgumentException("Wrong Connections preamble detected");
	}
        final String[] connectionsInput = line.replaceFirst(startSequence, "").split(",");

        Connection firstConnection = storeConnectionData(connectionsInput[0].trim());
        for (int i=1; i < connectionsInput.length-1; i++) {
            storeConnectionData(connectionsInput[i].trim());
	}
        Connection lastConnection = storeConnectionData(connectionsInput[connectionsInput.length-1].trim());

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Convert graph to dot format (https://en.wikipedia.org/wiki/DOT_(graph_description_language))
	// create the whole terminal line which can be used to generate the graph as ASCII graphic.
	// graphAsString += "}\"| graph-easy --from=dot --as_ascii";
        // System.out.println(graphAsString);
	////////////////////////////////////////////////////////////////////////////////////////////////
        // dot -T png graph.dot > graph.png
        // cat graph.dot | graph-easy --from=dot --as_ascii
	////////////////////////////////////////////////////////////////////////////////////////////////

	traverseFlightGraph(flights, firstConnection);
	printOutConnections();
    }

    private void printOutConnections() {
        System.out.println("Connections: ---------------------------------------------" );
        for(Map.Entry<String,Connection> entry : connections.entries()) {
            System.out.println("KEY:" + entry.getKey() + " --> " + entry.getValue().path().pathAsString() + "-" + entry.getValue().price());
        }
        System.out.println("----------------------------------------------------------" );
    }

    private Collection<Connection> getSameConnectionsWithDifferentPath(String start, String end) {
        Map<String,Collection<Connection>>map = connections.asMap();
        return map.get(start + "-" + end);
    }

    private Connection getExactThatConnectionsWithSamePath(String start, String end, Path path) {
        System.out.println("getExactThatConnectionsWithSamePath: " + start + " " + end + " " + path.pathAsString());
        Map<String,Collection<Connection>>map = connections.asMap();

        Collection<Connection> theConnections = map.get(start + "-" + end);
        Connection theConnection = null;
        if (theConnections != null && !theConnections.isEmpty()) {
            for(Connection connection: theConnections) {
                System.out.println("exactConnection: " + connection.path().pathAsString());
                System.out.println("matches with   : " + path.pathAsString());
                if (connection.path().pathAsString().equals(path.pathAsString())) {
                    theConnection = connection;
                    System.out.println("theConnection: " + theConnection.path().pathAsString());
                    System.out.println("        price: " + theConnection.price());
                }
                System.out.println("----------------------------------------------------------");
            }
        }
        System.out.println("##################################################################");
        return theConnection;
    }

    private void priceOfConnection(String start, String end, String data) {
        String answer = "";
        Collection<Connection>values = getSameConnectionsWithDifferentPath(start, end);
        if (values != null && !values.isEmpty()) {
            for(Connection connection: values) {
                if (connection.path().pathAsString().equals(data)) {
                    answer += connection.price() + " ";
                }
            }
        }
        if (answer.isEmpty()) {
            answer = "No such connection found!";
        }
        System.out.println(answer);
    }
    
    private void cheapestConnection(String start, String end) {
        String answer = "No such connection found!";
        Collection<Connection>values = getSameConnectionsWithDifferentPath(start, end);
        if (values != null && !values.isEmpty()) {
            Integer price = Integer.MAX_VALUE;
            for(Connection connection: values) {
                if (connection.price() < price) {
                    price  = connection.price();
                    answer = connection.path().pathAsString() + "-" + price;
                }
            }
        }
        System.out.println(answer);
    }
    
    private void howManyConnectionWithStops(Integer stops, String start, String end, int type) {
        String answer = "No such connection found!";
        Collection<Connection>values = getSameConnectionsWithDifferentPath(start, end);
        Integer connectionCounter = 0;
        if (values != null && !values.isEmpty()) {
            for(Connection connection: values) {
                if (type == PatternMatcher.CONNECTIONS_WITH_MAXIMUM_X_STOPS) {
                    if (connection.numberOfStops() <= stops) {
                        connectionCounter++;
                    }
                } else if (type == PatternMatcher.CONNECTIONS_WITH_EXACT_X_STOPS) {
                    if (connection.numberOfStops() == stops) {
                        connectionCounter++;
                    }
                } else if (type == PatternMatcher.CONNECTIONS_WITH_MINIMUM_X_STOPS) {
                    if (connection.numberOfStops() >= stops) {
                        connectionCounter++;
                    }
                } 
            }
        }
        System.out.println(connectionCounter.toString());
    }
    
    private void connectionsBelowPrice(String start, String end, Integer price) {
        String answer = "No such connection found!";
        Collection<Connection>values = getSameConnectionsWithDifferentPath(start, end);
        if (values != null && !values.isEmpty()) {
            for(Connection connection: values) {
                if (connection.price() < price) {
                    answer = connection.path().pathAsString() + "-" + connection.price();
                    System.out.println(answer);
                    // if (connection.start()) {
                    //     Collection<Connection>roundTrip = getSameConnectionsWithDifferentPath(connection.start(), connection.start());
                    //     if (roundTrip != null && !roundTrip.isEmpty() && roundTrip.roundTripPrice() > 0) {
                    //         final Connection cnnctn = connection.toBuilder().path().airports()addAllAirports(roundTrip.airports()).build();
                    //         answer = connection.path().pathAsString() + "-" + connection.price();
                    //         System.out.println(answer);
                    //     }
                    // }
                }
            }
        }
    }
    
    private void lookUpTheAnswer(Question question) {

	final Matcher matcher = question.matcher().getPreCompiledPattern(question.type(), true)
 	                                          .matcher(question.text());
	if (matcher.matches()) {
	    String start = "", end = "";
	    switch (question.type()) {
	    case PatternMatcher.PRICE_OF_CONNECTION:
		String path = matcher.group(1);
                start = path.substring(0, 3);
                end   = path.substring(path.length()-3);
                priceOfConnection(start, end, path);
		break;
	    case PatternMatcher.CHEAPEST_CONNECTION:
		start = matcher.group(1);
		end   = matcher.group(2);
                cheapestConnection(start, end);
		break;
	    case PatternMatcher.CONNECTIONS_BELOW_PRICE:
		start = matcher.group(1);
		end   = matcher.group(2);
                final Integer price = Integer.parseInt(matcher.group(3));
                connectionsBelowPrice(start, end, price);
		break;
	    case PatternMatcher.CONNECTIONS_WITH_MINIMUM_X_STOPS:
	    case PatternMatcher.CONNECTIONS_WITH_MAXIMUM_X_STOPS:
	    case PatternMatcher.CONNECTIONS_WITH_EXACT_X_STOPS:
                final Integer stops = Integer.parseInt(matcher.group(1));
                start = matcher.group(2);
		end   = matcher.group(3);
                howManyConnectionWithStops(stops, start, end, question.type());
		break;
	    case PatternMatcher.UNKNOWN:
	    case PatternMatcher.MAX:
	    default:
		break;
	    }
	}
    }

    public void answerQuestions(FileReader reader) {
	while (reader.hasNext()) {
            final String line = reader.nextLine();
	    //**  Build question and determine question type */ 
	    final Question question = new Question.Builder().text(line)
		                                            .matcher(patternMatcher)
		                                            .build();
	    System.out.println("Question: " + question.text());
	    lookUpTheAnswer(question);
        }
    }
}
