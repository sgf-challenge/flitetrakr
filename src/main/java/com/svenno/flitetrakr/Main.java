package com.svenno.flitetrakr;

import com.svenno.flitetrakr.utils.FileReader;
import com.svenno.flitetrakr.Service;
    
/**
 * FlightTrakr application to calculate flight prices and determine routes.
 */
public class Main {

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("usage: [java -jar flitetrakr.jar data]");
            System.exit(0);
        }

	FileReader reader = new FileReader(args[0]);
	Service   service = new Service();

	service.addConnections(reader);
	service.answerQuestions(reader);
        reader.close();
    }
}
