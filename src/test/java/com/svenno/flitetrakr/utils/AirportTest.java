package com.svenno.flitetrakr.utils;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import com.svenno.flitetrakr.utils.Airport;

public class AirportTest {

    final Airport departure;

    public AirportTest() {
        departure = new Airport.Builder().iataCode("LHR").build();
    }
    
    @Test
    public void Test1() {
        final Airport result = new Airport.Builder()
            .iataCode("LHR")
            .build();
        
        assertThat(result.toString(), is(equalTo(departure.toString())));
        assertThat(result.iataCode(), is(equalTo(departure.iataCode())));
    }

    @Test
    public void Test2() {
        Airport result = null;
	try {
	    result = new Airport.Builder().build();
	} catch (IllegalStateException e) {
	    assertThat(result, is(nullValue()));
	}
    }

    @Test
    public void Test3() {
        Airport result = null;
	try {
	    result = new Airport.Builder().iataCode("").build();
	} catch (IllegalArgumentException e) {
	    assertThat(result, is(nullValue()));
	}
    }

    @Test
    public void Test4() {
        Airport result = null;
	try {
	    result = new Airport.Builder().iataCode(null).build();
	} catch (NullPointerException e) {
	    assertThat(result, is(nullValue()));
	}
    }
}
