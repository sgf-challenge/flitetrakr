package com.svenno.flitetrakr.utils;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import com.svenno.flitetrakr.utils.Path;
import com.svenno.flitetrakr.utils.Airport;

public class PathTest {

    final Airport departure;
    final Airport arrival;
    final Airport stop;
    
    public PathTest() {
        departure = new Airport.Builder().iataCode("LHR").build();
        arrival   = new Airport.Builder().iataCode("AMS").build();
        stop      = new Airport.Builder().iataCode("NUE").build();
    }
    
    @Test
    public void Test1() {
        final String path = new String("LHR-AMS");
        final Path result = new Path.Builder()
            .addAirports(departure, arrival)
            .build();
        assertThat(result.pathAsString(), is(equalTo(path)));
    }

    @Test
    public void Test2() {
        final String path = new String("LHR-NUE-AMS");
        final String connection = new String("LHR-AMS");
        final Path result = new Path.Builder()
            .addAirports(departure, stop, arrival)
            .build();
        
        assertThat(result.pathAsString(), is(equalTo(path)));
        assertThat(result.connectionAsString(), is(equalTo(connection)));
    }

    @Test
    public void Test3() {
        Path result = null;
	try {
	    result = new Path.Builder().build();
	} catch (IllegalArgumentException e) {
	    assertThat(result, is(nullValue()));
	}
    }

    @Test
    public void Test4() {
        Path result = null;
        Airport airport = null;
	try {
	    result = new Path.Builder().addAirports(airport).build();
	} catch (NullPointerException e) {
	    assertThat(result, is(nullValue()));
	}
    }
}
