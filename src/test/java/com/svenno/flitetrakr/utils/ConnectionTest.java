package com.svenno.flitetrakr.utils;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import com.svenno.flitetrakr.utils.Connection;

public class ConnectionTest {

    final Integer price;
    final Integer roundTripPrice;
    final Integer stops;
    final Path    path;
    final Airport departure;
    final Airport arrival;

    public ConnectionTest() {
        price     = 83;
        roundTripPrice = 0;
        stops     = 2;
        departure = new Airport.Builder().iataCode("LHR").build();
        arrival   = new Airport.Builder().iataCode("AMS").build();
        path      = new Path.Builder().addAirports(departure, arrival).build();
    }

    @Test
    public void Test1() {
        final Connection result = new Connection.Builder()
                                             .start(departure)
	                                     .end(arrival)
	                                     .path(path)
	                                     .price(price)
	                                     .roundTripPrice(roundTripPrice)
	                                     .numberOfStops(stops)
	                                     .build();
        assertThat(result.start(),           is(equalTo(departure)));
        assertThat(result.end(),             is(equalTo(arrival)));
        assertThat(result.path(),            is(equalTo(path)));
        assertThat(result.price(),           is(equalTo(price)));
        assertThat(result.roundTripPrice(),  is(equalTo(roundTripPrice)));
        assertThat(result.numberOfStops(),   is(equalTo(stops)));
    }
}
