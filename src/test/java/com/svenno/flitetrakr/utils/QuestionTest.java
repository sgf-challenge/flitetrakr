package com.svenno.flitetrakr.utils;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import com.svenno.flitetrakr.utils.PatternMatcher;
import com.svenno.flitetrakr.utils.Question;

public class QuestionTest {

    /** all kind of known questions as text */
    final String[] texts = {
	"What is the price of the connection NUE-FRA-LHR?",
	"What is the cheapest connection from NUE to AMS?",
	"How many different connections with maximum 3 stops exists between NUE and FRA?",
	"How many different connections with minimum 1 stop exist between FRA and LHR?",
	"How mand different connections with exactly 1 stop exists between LHR and AMS?",
	"Find all conenctions from NUE to LHR below 170Euros!",
	"This is fake news!"
    };

    /** all known question types as type */
    final int[] questionType = {
	PatternMatcher.PRICE_OF_CONNECTION,
	PatternMatcher.CHEAPEST_CONNECTION,
	PatternMatcher.CONNECTIONS_WITH_MAXIMUM_X_STOPS,
	PatternMatcher.CONNECTIONS_WITH_MINIMUM_X_STOPS,
	PatternMatcher.CONNECTIONS_WITH_EXACT_X_STOPS,
	PatternMatcher.CONNECTIONS_BELOW_PRICE,
	PatternMatcher.UNKNOWN
    };

    private final PatternMatcher patternMatcher;
    
    public QuestionTest() {
	patternMatcher = new PatternMatcher(); 
    }
    
    @Test
    public void Test1() {
	for (int i=0; i<texts.length; i++) {
	    Question result = new Question.Builder().text(texts[i]).matcher(patternMatcher).build();
	    assertThat(result.text(), is(not(nullValue())));
	    assertThat(result.text(), is(equalTo(texts[i])));
	    assertThat(result.type(), is(equalTo(questionType[i])));
	}
    }

    @Test
    public void Test2() {
	Question result = null;
	String txt = "";
	try {
	    result = new Question.Builder().text(txt).matcher(patternMatcher).build();
	} catch (IllegalArgumentException e) {
	    assertThat(result, is(nullValue()));
	}
    }

    @Test
    public void Test3() {
	Question result = null;
	String txt = null;
	try {
	    result = new Question.Builder().text(txt).matcher(patternMatcher).build();
	} catch (NullPointerException e) {
	    assertThat(result, is(nullValue()));
	}
    }

    @Test
    public void Test4() {
	Question result = null;
	String txt = "";
	try {
	    result = new Question.Builder().matcher(patternMatcher).build();
	} catch (IllegalStateException e) {
	    assertThat(result, is(nullValue()));
	}
    }
}
